/**
 * @file
 * Adds breadcrumb dropdown javascript.
 */

(function ($) {
  'use strict';

  /**
   * Adds the dropdown functionality.
   */
  Drupal.behaviors.breadcrumbDropdown = {
    attach: function (context, settings) {
      var $breadcrumb = $('#breadcrumb-dropdown');

      if (!$breadcrumb.data('toggleSlide')) {
        $breadcrumb.find('> li > a, > li > span').click(function (e) {
          var $this = $(this);
          var $target = $this.siblings('ul');

          // Make sure that the back button works.
          if (!$this.hasClass('home')) {
            e.preventDefault();
          }

          // Slide up all visible menus.
          $breadcrumb.find('> li > ul:visible').not($target).slideUp(200);

          // Toggle the target.
          $target.slideToggle(200);
        });

        $breadcrumb.data('toggleSlide', 1);
      }
    }
  };
})(jQuery);
